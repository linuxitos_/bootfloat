<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Inputs flotas</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
		<!--link rel="stylesheet" href="https://bootswatch.com/5/quartz/bootstrap.min.css" -->
		<link href="custom.css" rel="stylesheet">
		<link rel="icon" href="favicon.png"/>
	</head>
	<body>
		<div class="container">
			<header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
				<div class="col-md-3 mb-2 mb-md-0">
					<a href="/" class="d-inline-flex link-body-emphasis text-decoration-none">
						<img src="./logo.svg" alt="" width="150">
					</a>
				</div>

				<ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
					<li><a href="#" class="nav-link px-2 link-secondary">Home</a></li>
				</ul>

				<div class="col-md-3 text-end">
					<button type="button" class="btn btn-outline-primary me-2">Login</button>
					<button type="button" class="btn btn-primary">Sign-up</button>
				</div>
			</header>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="card border">
					<div class="card-header">
							<h3 class="card-title">
								Login
							</h3>
						</div>
						<div class="card-body">
							<form class="form-login" id="form-login" name="form-login" method="post">
								<div id="div-cnt-login" class="row justify-content-center align-items-center">
									<div class="col-md-12">
										<div class="input-group mb-3">
											<span class="has-float-label">
												<input type="text" class="form-control" name="username" autofocus="" autocomplete="username" maxlength="150" required="required" id="txtUser" placeholder=" ">
												<label for="txtUser">Usuario</label>
												<i class="bi bi-person-circle icon-form"></i>
											</span>
										</div>
									</div>
									<div class="col-md-12 mb-3">
										<div class="input-group">
											<span class="has-float-label">
												<input type="password" class="form-control" name="password" autocomplete="current-password" required="required" id="id_password" placeholder=" ">
												<label for="id_password">Contraseña</label>
												<i class="bi bi-lock icon-form"></i>
											</span>
										</div>
									</div>
									<div class="col-md-12 mb-3">
										<button id="btn-login" type="submit" class="btn btn-success form-login float-end">
											<i class="bi bi-check-circle"></i> Iniciar
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">
								Registro
							</h3>
						</div>
						<div class="card-body">
							<form id="form-signup" name="form-signup" class="form-signup" method="post">
								<div id="div-cnt-signup" class="row justify-content-center align-items-center">
									<div class="col-md-12">
										<div class="input-group mb-3">
											<span class="has-float-label">
												<input class="form-control" type="text" name="username" maxlength="150" autocapitalize="none" autocomplete="off" autofocus=""
													required="required" id="txtUserReg" placeholder=" ">
												<label for="txtUserReg">Usuario</label>
												<i class="bi bi-person-circle icon-form"></i>
											</span>
										</div>
									</div>
									<div class="col-md-12 mb-3">
										<div class="input-group">
											<span class="has-float-label">
												<input class="form-control" type="password" name="password1" autocomplete="off" required="required" id="id_password1"
													aria-autocomplete="list" placeholder=" ">
												<label for="id_password1">Contraseña</label>
												<i class="bi bi-lock icon-form"></i>
											</span>
										</div>
									</div>
									<div class="col-md-12 mb-3">
										<div class="input-group">
											<span class="has-float-label">
												<input class="form-control" type="password" name="password2" autocomplete="off" required="required" id="id_password2" placeholder=" ">
												<label for="id_password2">Confirmar contraseña</label>
												<i class="bi bi-lock icon-form"></i>
											</span>
										</div>
									</div>
									<div class="col-md-12 mb-3">
										<button id="btn-signup" type="submit" class="btn btn-success form-signup float-end">
											<i class="bi bi-check-circle"></i> Registrarse
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
	</body>
</html>